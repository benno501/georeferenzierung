# Convert Microsoft Word 'doc' files to 'docx' format by opening and
# saving Word files using win32com to automate Microsoft Word.
#
# The script walks a directory structure and converts all '.doc' files found.
# Original 'doc' and new 'docx' files are saved in the same directory.
#
# This Word automation method has been found to work where OFC.exe and
# wordconv.exe do not.
#
# Tested using Windows 7, Word 2013, python 2.7.10, pywin32-219.win-amd64-py2.7

import os.path
import win32com.client


def convert_doc2docx(doc_file):
    """Diese Funktion öffnet eine *.doc Datei und speichert sie als *.docx Datei ab.
	Die docx-Dateien werden in: "T:\PublicTS\Perchermeier\GutachtenAlsDocx" unter dem relativen Dateinamen abgelegt.
	Beispiel:
	nameNpath = 'T:\\Projekte\\Auftraege_2019\\19100130_Neuoetting_Fischervorstadt\\Gutachten\\Gutachten_Entwurf.doc'
	wird als: 'T:\\Programmdaten\\GeoreferenzierungProjekte\\GutachtenAlsDocx\\Projekte\\Auftraege_2019\\
				19100130_Neuoetting_Fischervorstadt\\Gutachten\\Gutachten_Entwurf.docx'
	neu abgespeichert.
	"""
    # Falls der Pfrad zu der Datei zu lang ist, überspringe Umwandlung
    if len(doc_file) < 200:
        # Extrahiere Dateiendung
        _, file_extension = os.path.splitext(doc_file)

        # Mache Umwandlung nur, wenn Dateiendung == .doc
        if file_extension.lower() == '.doc':
            # Erstelle Pfad und Dateinamen von neu zu erstellenden docx Datei:
            _, path_tail = os.path.splitdrive(doc_file)
            final_file = os.path.join(r"T:\Programmdaten\ProjekteGeoreferenziert\GutachtenAlsDocx",
                                      path_tail[1:])
            output_folder, _ = os.path.split(final_file)

            # Füge "x" zu ".doc" hinzu --> "docx"
            docx_file = '{0}{1}'.format(final_file, 'x')

            # Überspringe die ganze Prozedur, falls eine Datei mit dem gleichen Namen im gleichen Ordner bereits vorhanden ist.
            if not os.path.isfile(docx_file):  # Skip conversion where docx file already exists
                print('Converting: {0}'.format(doc_file))

                # Erstelle neuen Ordner, falls noch nicht vorhanden.
                if not os.path.isdir(output_folder):
                    try:
                        os.makedirs(output_folder)
                    except Exception:
                        print("Failed to create Folder.")

                # Starte word
                word = win32com.client.Dispatch("Word.application")

                # Öffne alte *.doc Datei und erstelle neue *.docx Datei
                try:
                    # öffnen
                    wordDoc = word.Documents.Open(doc_file, False, False, True)
                    # speichern in aktuellem Dateiformat
                    wordDoc.SaveAs2(docx_file, FileFormat=16)
                    # schließen
                    wordDoc.Close()
                    print("Successfully converted Gutachten")
                except Exception:
                    print('Failed to Convert')

                # Beende word
                word.Quit()

            else:
                print(
                    '        - corresponding docx file in folder T:\PublicTS\Perchermeier\GutachtenAlsDocx already exists'.format(
                        doc_file))

            return docx_file
    else:
        print('error: path too long\n')


# ---------------------------------------------------------------------------------------------
# Teste convert_doc2docx() mit einzelner Datei
if __name__ == '__main__':
    Beispieldatei = r'T:\Projekte\Auftraege_2017\17010168_Sanierung B308Jochpass\Ausgang_intern\20190719_Junge_Übergabe_LV\Ausschreibung Gesamtsanierung\90_Ausschreibung_in_Bauabschnitten_Bereiche\Schutzgalerie\Ausschreibung\Vorgez_Maßnahme\0_Vorprüfung_Angebote1\Stellungnahme_Fragen_30_08_13.doc'
    convert_doc2docx(Beispieldatei)
