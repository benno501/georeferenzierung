# importiere packages
import csv
import simplekml
# importiere andere Funktionen
from getAdressWord import word2dict
from findGutachten import find_Gutachten
from accessWebsiteGeocoder import get_coords
from itertools import chain

# Wichtige Parameter
google_api_key = 'AIzaSyDL-GAGOs1ufaqWLdn6J2D_Dc3qriB5KAI'
name_csv_datei = "GeoreferenzierteAuftraege.csv"
name_kml_datei = "GeoreferenzierteAuftraege.kml"

# Ordnerparameter
Ordner = r"T:\Projekte\Auftraege_2019\19100184_Grassau_Piesenhausener_Str"

# Finde alle Word Dokumente, die Gutachten zu sein scheinen.
paths_Gutachten_corrected, original_paths, DokumentArt = find_Gutachten(Ordner)

# Erstelle leere kml Datei
kml = simplekml.Kml()

# Erstelle leere Liste für duplikatkontrolle
finished_List = []

print("\n\nGeoreferenziere Gutachten...\n")

# Öffne CSV-Datei
with open(name_csv_datei, mode='w') as coords_file:
    # Einstellungen zum CSV-schreiben (";" als Trennzeichen)
    coords_writer = csv.writer(coords_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    # Benamung erste Zeile (Spaltenbenamung)
    first_row = ['AZ', 'Ort_Datum', 'Art der Stellungnahme', 'Bauvorhaben', 'latitude', 'longitude', 'Bauherr', 'Planer', 'Statiker', 'Auftraggeber', 'Sachbearbeiter', 'Bodenschichten', 'Dateipfad']
    # Schreibe erste Zeile in Csv Datei
    coords_writer.writerow(first_row)
    # Gehe durch die Docx Dokumente, die über find_Gutachten() gefunden wurden
    for i, path in enumerate(paths_Gutachten_corrected):
        original_path = original_paths[i]
        print(original_path)
        # Extrahiere Informationen
        my_dict = word2dict(path)
        Bauvorhaben = my_dict['Bauvorhaben']
        # Falls der Wert für Bauvohaben nicht leer ist, google den Wert "Bauvorhaben"
        if Bauvorhaben:
            lat, lon = get_coords(Bauvorhaben, google_api_key)
            result = [my_dict['AZ'], my_dict['Ort_Datum'], DokumentArt[i], Bauvorhaben, lat, lon, my_dict['Bauherr'], my_dict['Planer'], my_dict['Statiker'], my_dict['Auftraggeber'], my_dict['Sachbearbeiter'], my_dict['Bodenschichten'], original_paths[i]]
            try:
                # schreibe die Ergebnisse in die CSV Datei
                coords_writer.writerow(result)
            except Exception:
                # falls sich ein Problem bei dem Schreiben ergibt, setz die Zeichenkodierung auf utf-8
                coords_writer.writerow([entry.encode("utf-8") for entry in result if isinstance(entry, str)])
            # falls das Gutachtne so noch nicht vorhanden ist, addiere es zu der finalen Liste
            if result not in finished_List:
                finished_List.append(result)
                # Schreibe die PArameter um, damit sie besser zum KML Format passen
                result_Newlines = [str(s) + "\n" for s in result]
                first_row_kml = first_row[:]
                # Lösche latitude und longitude aus kml Beschreibung (Doppeltgemoppelt)
                del result_Newlines[3:5]
                del first_row_kml[3:5]
                description = list(chain.from_iterable(zip(first_row_kml, result_Newlines)))
                description_string = "\n ".join(description)
                # Schreibe neuen Punkt in KML Datei
                kml.newpoint(name=Bauvorhaben, coords=[(lon, lat)], description=description_string)
            else:
                print(" -------------------- Kein neuer Eintrag hinzugefügt, da bereits vorhanden.")
        else:
            print(" -------------------- Überspringe Datei da Bauvorhaben nicht angegeben: -----------------\n" + str(original_paths[i]))

# Speichere KML Datei
kml.save(name_kml_datei)

print('done')
