import os
from doc2docx import convert_doc2docx


def find_Gutachten(search_folder, folder_age=None):
    """Durchsuche Ordnerstruktur nach Gutachen, Baugrundgutachten, Stellungnahmen usw.
    Ausschlaggebens sind drei Parameter:
    filter_words_positive, was eine Datei im Namen haben muss
    filter_words_negative, was eine Datei nicht im Namen haben darf und
    folder_age, was bestimmt ob die neueren oder die älteren Ordner durchsucht werden
        folder_age = "alt" bedeutet alle älteren Ordner werden durchsucht: Auftraege_001_199 bis Auftraege_700_749
        folder_age = "neu" bedeutet alle neueren Ordner werden durchsucht: Auftraege_2010 bis Auftraege_2019

    Die Funktion gibt folgende Parameter aus:
    GoodFilesList = Eine Liste der Dateipfade der docx Dateien
    AllFilesList = Eine Liste der originalen Dateipfade der Word Dateien
    DokumentenArtListe = Eine Liste des jeweilig zuerst gefundenen Stichwortes (filter_word_positive)"""

    # Wörter die im Dateinamen stehen müssen:
    filter_words_positive = ["Gutachten", "Baugrundgutachten", "Stellungnahme", "Kurzstellungnahme",
                             "Ergaenzungsbericht", "Ergänzungsbericht", "Vorbericht", "bericht", "stellungnahme",
                             "Baugrund", "gutachten"]
    # Wörter die nicht im Dateinamen stehen dürfen:
    filter_words_negative = ["~$", "wip", "Vorlage", "Entwurf", "Statikbericht", "ENTWURF", "tagesbericht"]

    # Liste von Ordnern, die durchsucht werden sollen. In diesem Fall nur für Auftraege 2010 bis Auftraege 2019
    if folder_age is None:
        auftraege_folders = [search_folder]
    elif "alt" in folder_age:
        # In diesem Fall nur für Auftraege_001_199 bis Auftraege_700_749
        auftraege_folders = [f.path for f in os.scandir(search_folder) if
                             f.is_dir() and "Auftraege_" in f.path and "Auftraege_201" not in f.path and
                             "_BG_" not in f.path]
    elif "neu" in folder_age:
        # In diesem Fall nur für Auftraege 2010 bis Auftraege 2019
        auftraege_folders = [f.path for f in os.scandir(search_folder) if
                             f.is_dir() and "Auftraege_201" in f.path]
    else:
        raise ValueError("Der Parameter für das Ordneralter konnte nicht bestimmt werden. 'folder_age' ist fehlerhaft.")

    # Starte leere Parameterlisten
    GoodFilesList = []
    AllFilesList = []
    DokumentenArtListe = []

    # Durchsuche alle Ordner
    for auftrag_folder in auftraege_folders:
        for path, dirs, filenames in os.walk(auftrag_folder):
            # Untersuche Dateinamen in Ordner
            for f in filenames:
                # Untersuche Dateinamen auf positive und negative Stichwörter
                if any(filter_word in f for filter_word in filter_words_positive) and any(filter_word not in f for filter_word in filter_words_negative):
                    # Extrahiere erstes positives Filterwort
                    positive_filter_word = next((x for x in filter_words_positive if x in f), False)

                    # Kontrolliere ob sich Datei im neuen Dateiformat docx befindet
                    if f.endswith((".docx")):
                        # addiere Pfad der Datei zu Dateienliste, falls sie nicht bereits in der Liste vorhanden ist.
                        GoodFilePath = os.path.join(path, f)
                        if GoodFilePath not in GoodFilesList:
                            print(str(GoodFilePath) + " ------- is perfectly fine :)")
                            GoodFilesList.append(GoodFilePath)
                            AllFilesList.append(GoodFilePath)
                            DokumentenArtListe.append(positive_filter_word)
                    elif f.endswith((".doc")):
                        # addiere Pfad der Datei zu Dateienliste, falls sie nicht bereits in der Liste vorhanden ist.
                        BadFilePath = os.path.join(path, f)
                        if BadFilePath not in AllFilesList:
                            print(str(BadFilePath) + " ------- is a shitty old file")
                            # Wandle doc Dateien zu docx um
                            new_pathNFile = convert_doc2docx(BadFilePath)
                            GoodFilesList.append(new_pathNFile)
                            AllFilesList.append(BadFilePath)
                            DokumentenArtListe.append(positive_filter_word)
        result = GoodFilesList, AllFilesList, DokumentenArtListe
    return result


# ---------------------------------------------------------------------------------------------
# Teste  find_Gutachten
if __name__ == '__main__':
    # Suche in Ordner Projekte:
    search_folder_in = r"T:\Projekte"
    ordner_alter = "neu"
    find_Gutachten(search_folder_in, ordner_alter)

