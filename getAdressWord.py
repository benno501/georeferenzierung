import docx

def word2dict(WordFileName):
    """Extrahierung der Informationen von der Titelseite und der Böden von Tabelle 1 """

    # "Öffne" Word Dokument über Dateipfad
    Dateipfad = WordFileName
    document = docx.Document(Dateipfad)

    # Starte leere Parameter
    Bauvorhaben_list = []
    Bauherr_list = []
    Auftraggeber_list = []
    Planer_list = []
    Statiker_list = []
    Sachbearbeiter_list = []
    # Falls keine Informationen verfügbar sind werden die folgenden Parameter als "nicht bekannt" festgesetzt,
    # andernfalls werden sie einfach im nachfolgendne Verlauf überschrieben
    AZ = "nicht bekannt"
    Ort_Datum = "nicht bekannt"
    Bodenschichten = "nicht bekannt"

    # ------------------- 1. Lese Titelblatt aus --------------------------------------

    # Kontrolliere ob Dokument überhaupt Tabellen hat
    if document.tables:
        # lese allererste Tabelle aus. Das ist im Regelfall das Titelblatt
        titelblattTabelle = document.tables[0]
        # Starte leeren Parameter
        a = " "
        # Lese Die Tabelle Zelle für Zelle aus
        for row in titelblattTabelle.rows:
            for cell in row.cells:
                # Kontrolliere ob Zelltext gewisse Stichwörter enthält, wie z.B. "Bauvorhaben"
                if cell.text:
                    if cell.text == "Bauvorhaben" or cell.text == "Bauherr" or cell.text == "Planer" or cell.text == "Auftraggeber" or cell.text == "Statiker" or cell.text == "Sachbearbeiter":
                        # Setze Parameter a gleich dem Zelltext, z. B. "Bauvorhaben"
                        a = cell.text
                    # Verhindere, dass Zelltexte wie ":" oder leere Zelltexte ausgelesen werden
                    if cell.text != ":" and cell.text != a and cell.text != "":
                        # entferne untrennbare Leerzeichen von Zelltext
                        if str("\xa0") in cell.text:
                            cell.text = cell.text.replace("\xa0", " ")

                        # Erstelle einzelne Listen für die einzelnen Kathegorien
                        if a == "Bauvorhaben":
                            Bauvorhaben_list.append(cell.text)
                        elif a == "Bauherr":
                            Bauherr_list.append(cell.text)
                        elif a =="Auftraggeber":
                            Auftraggeber_list.append(cell.text)
                        elif a == "Planer":
                            Planer_list.append(cell.text)
                        elif a == "Statiker":
                            Statiker_list.append(cell.text)
                        elif a == "Sachbearbeiter":
                            # Da Arbeitszeichen und Ort_Datum am Schluss der Tabelle stehen werden sie hier ausgewertet
                            # und als eigene Parameter aufgenommen
                            if 'AZ' in cell.text:
                                AZ = cell.text
                            elif 'Traunstein' in cell.text or "München" in cell.text:
                                Ort_Datum = cell.text
                            else:
                                Sachbearbeiter_list.append(cell.text)

        # ------------------- 2. Extrahiere Bodenschichten -----------------------------------
        # Gehe alle Tabellen durch und suche nach "Tabelle 1" bzw. "Tabelle 1.1", die die Bodenkennwerte enthält.

        # Anzahl Tabellen in Worddatei
        numerOfTablesInDocument = len(document.tables)
        # Teste jede einzelne Tabelle ob die Überschrift (caption) "Tabelle 1" enthält:
        for i in range(numerOfTablesInDocument):
            testing_table = document.tables[i]
            try:
                caption = testing_table._cells[000].text
            except Exception:
                caption = "nicht auslesbar"
            # Nach Ausschlussprinzip wird die Tabelle nur berücksichtigt, die "Tabelle 1" enthält, aber nicht
            # "Tabelle 1.2" heißt
            if "Tabelle 1" in caption and "Tabelle 1.2" not in caption:
                # Anzahl der Spalten
                cols = testing_table._column_count
                # Extrahiere die Werte der Bodenschicht (die Werte sind quasi die unterste Zeile in der Tabelle,
                # da die Tabelle um 90 Grad gedreht ist)
                # Definiere Zellen zum auslesen (letzte Reihe)
                my_values = testing_table._cells[-cols + 2:-1]
                # Lese Zelle für Zelle aus
                Bodenschichten_list = [entry.text for entry in my_values]
                # Entferne die unnötigen Absätze
                Bodenschichten_list_corrected = [w.replace('\n', ' ') for w in Bodenschichten_list]
                # Addiere Absätze wieder, wo sie angebracht sind ;) (quasi so, wie sie im Dokument auch sind)
                Bodenschichten = "\n ".join(Bodenschichten_list_corrected)

    # Entferne doppelte Einträge
    Bauvorhaben = " ".join(list(dict.fromkeys(Bauvorhaben_list)))
    Bauherr = " ".join(list(dict.fromkeys(Bauherr_list)))
    Auftraggeber = " ".join(list(dict.fromkeys(Auftraggeber_list)))
    Planer = " ".join(list(dict.fromkeys(Planer_list)))
    Statiker = " ".join(list(dict.fromkeys(Statiker_list)))
    Sachbearbeiter_list = " ".join(list(dict.fromkeys(Sachbearbeiter_list)))

    # Exportiere alle Informationen als Python Dictionary
    ExportInfo = dict(AZ=AZ, Ort_Datum=Ort_Datum, Bauvorhaben=Bauvorhaben, Bodenschichten=Bodenschichten, Bauherr=Bauherr,
                      Planer=Planer, Statiker=Statiker, Auftraggeber=Auftraggeber, Sachbearbeiter=Sachbearbeiter_list,
                          Dateipfad=Dateipfad)

    return ExportInfo

# ---------------------------------------------------------------------------------------------
# Teste getAdressWord mit einzelner Datei
if __name__ == '__main__':
    fname = r'T:\Programmdaten\GeoreferenzierungProjekte\GutachtenAlsDocx\Projekte\Auftraege_2019\19100206_Grassau_Mietenkamerstraße_8\Gutachten\Gutachten_18_09_19.docx'
    word2dict(fname)


