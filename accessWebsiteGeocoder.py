import geocoder

def get_coords(adress, api_key):
    """Fügt im Prinzip die gegebene adresse bei Google Maps ein und gibt latitude und logitude zurpüpc"""
    # Um eine Anfrage an google zu schicken muss ein API key erstellt werden. Dazu braucht man ein google Konto und
    # muss dort seine Zahlungsinformationen hinterlegen. Man bekommt $200-300 Guthaben, was locker ausreicht.
    # link zu api anleitung: https://developers.google.com/maps/documentation/geocoding/get-api-key
    g = geocoder.google(adress, key=api_key)
    # nur Latitude und Longitude sind von Interesse
    coordinates = g.latlng
    if coordinates is None:
        coordinates = (None, None)
    return coordinates


if __name__ == '__main__':
    adresse ='Osterwinkelstr. 8b Chieming'
    google_api_key = 'AIzaSyDL-GAGOs1ufaqWLdn6J2D_Dc3qriB5KAI'
    get_coords(adresse, google_api_key)

