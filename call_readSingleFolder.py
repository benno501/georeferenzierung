# importiere andere Funktionen
from create_map import create_map
from findGutachten import find_Gutachten

# Wichtige Parameter
google_api_key = 'AIzaSyDL-GAGOs1ufaqWLdn6J2D_Dc3qriB5KAI'
name_csv_datei = "GeoreferenzierteAuftraege.csv"
name_kml_datei = "GeoreferenzierteAuftraege.kml"

# Ordnerparameter
Ordner = r"T:\Projekte\Auftraege_2019\19100184_Grassau_Piesenhausener_Str"

# Finde alle Word Dokumente, die Gutachten zu sein scheinen.
paths_Gutachten_corrected, original_paths, DokumentArt = find_Gutachten(Ordner)

create_map(google_api_key, name_csv_datei, name_kml_datei, Ordner, paths_Gutachten_corrected,
               original_paths, DokumentArt)

